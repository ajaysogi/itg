module.exports = {
	vehicles: [
		{
			id: "xe",
			modelYear: "k17",
			description: "The most advanced, efficient and refined sports saloon that Jaguar has ever produced",
			price: "£30,000",
			url: "/api/vehicle/xe",
			media: [
				{
					name: "vehicle",
					url: "/images/xe_k17.jpg"
				}
			]
		},
		{
			id: "xf",
			modelYear: "k17",
			description: "Luxury business saloon with distinctive design, dynamic drive and state-of-the-art technologies.",
			price: "£36,000",
			url: "/api/vehicle/xf",
			media: [
				{
					name: "vehicle",
					url: "/images/xf_k17.jpg"
				}
			]
		},
		{
			id: "xj",
			modelYear: "k16",
			description: "Premium luxury saloon, spacious and beautiful yet powerfully agile.",
			price: "£50,000",
			url: "/api/vehicle/xj",
			media: [
				{
					name: "vehicle",
					url: "/images/xj_k16.jpg"
				}
			]
		},
		{
			id: "fpace",
			modelYear: "k17",
			description: "Jaguar's luxury performance SUV.",
			price: "£40,000",
			url: "/api/vehicle/fpace",
			media: [
				{
					name: "vehicle",
					url: "/images/fpace_k17.jpg"
				}
			]
		},
		{
			id: "ftype",
			modelYear: "k17",
			description: "Pulse-quickening, pure Jaguar sports car.",
			price: "£60,000",
			url: "/api/vehicle/ftype",
			media: [
				{
					name: "vehicle",
					url: "/images/ftype_k17.jpg"
				}
			]
		}
	]
}