# ITG Frontend Test

## Overview
This challenge I completed using Vue.js and the Vue CLI. I moved across the code from the express server files from the challenge and hosted them within the CLI to be able to interact better and keep all files centralised.

To achieve the look of the designs supplied, I noticed there wasn't enough mock data available so I don't know if this was a bug or intentional, however i tweaked some of the mock data to be able to make more efficient requests. For example when displaying the price and description on the cards; this data was not present in the ```api/vehicles``` path. It seemed like bad practice having to fire multiple requests to the API just to retrieve this information from ```api/vehicles/{$id}```. This information i felt should be present in the ```api/vehicles``` path, therefore i made the tweak to tackle the multiple API request issue.


## Project setup
```
npm install
```

### Run Express Server & Serve Vue Files:
```
npm run start
```

*IF FAILED: Run each command below individually.*

#### Compiles and hot-reloads for development
```
npm run serve
```

#### Run Express Server
```
npm run express:run
```

## Node Version Required
```
Node 14.8.0
```


## Author
**Name:** Ajay Sogi\
**Email:** a.sogi24@gmail.com 
