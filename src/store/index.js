import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios';

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    cars: null
  },
  mutations: {
    setCars(state, payload) {
      state.cars = payload
    },
    setCarDetails(state, payload) {
      const { cars } = state;
      const { id } = payload;
      const selectedCar = cars.find(f => f.id === id);
      const result = cars.map(car => {
        return selectedCar.id === car.id ? { ...selectedCar, detail: payload } : car;
      });
      state.cars = result;
    }
  },
  actions: {
    async loadCars ({ commit }) {
      try {
        const response = await axios.get('http://localhost:3000/api/vehicle')
        commit('setCars', response.data.vehicles);
      } catch (e) {
        console.error(e);
      }
		},
    async loadCarDetailsById ({ dispatch, commit, state }, payload) {
      try {
        const id = payload;
        const response = await axios.get(`http://localhost:3000/api/vehicle/${id}`);
        const { cars } = state;
        if (!cars) await dispatch('loadCars');
        commit('setCarDetails', response.data);
      } catch (e) {
        console.error(e);
      }
    }
  },
  modules: {
  },
  getters: {
    cars: state => state.cars
  }
})
